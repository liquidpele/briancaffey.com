Description: >

  This template deploys a VPC, with a pair of public and private subnets spread
  across two Availabilty Zones. It deploys an Internet Gateway, with a default
  route on the public subnets.

  It then deploys a highly available ECS cluster using an AutoScaling Group, with
  ECS hosts distributed across multiple Availability Zones.

  Finally, it deploys a Django application and a celery worker from containers
  published in Amazon EC2 Container Registry (Amazon ECR).

  It also deploys a static site with S3 and CloudFront.

  Last Modified: 24th March 2019
  Author: Brian Caffey

Parameters:

  StackName:
    Type: String
    Description: The name of the stack

  EnvironmentName:
    Type: "String"
    Description: An environemnt name to namespace resources (e.g. qa, staging, production)

  AppUrl:
    Type: "String"
    Description: "The URL for our app (e.g. mydomain.com)"
    AllowedPattern: "[a-z0-9._-]+"

  HostedZoneId:
    Type: AWS::Route53::HostedZone::Id
    Description: "The hosted zone ID to add the Route 53 recordset to."

  SSLCertificateArn:
    Type: "String"
    Description: "ARN pointing to an SSL cert to use for this app URL."
    AllowedPattern: "arn:aws:acm:.*"
    ConstraintDescription: "Please supply a valid ARN to a certificate."

  WildcardSSLCertificateArn:
    Type: "String"
    Description: "ARN pointing to a wildcard SSL cert to use for this app URL."
    AllowedPattern: "arn:aws:acm:.*"
    ConstraintDescription: "Please supply a valid ARN to a wildcard certificate."

  ECRBackendRepositoryName:
    Type: "String"
    Description: "Name of ECR Repository for backend web server."

  SSHKeyName:
    Type: "String"
    Description: The name of the key-pair used for SSH access to ECS hosts

  GitSHA:
    Type: "String"
    Description: "This is a test parameter used to see how to pass parameters from GitLab"

  FlowerUsername:
    Description: "Username for flower monitoring service authentication"
    Type: String
    NoEcho: true

  FlowerPassword:
    Description: "Password for flower monitoring service authentication"
    Type: String
    NoEcho: true

  AWSAccessKeyId:
    Description: "AWS ACCESS KEY ID"
    Type: String
    NoEcho: true

  AWSSecretAccessKey:
    Description: "AWS SECRET ACCESS KEY"
    Type: String
    NoEcho: true

  DjangoSecretKey:
    Description: The Secret Key for backend, celery and beat containers
    Type: String
    NoEcho: true

Conditions:
  RunServices: !Equals [ 1, 1 ]
  UseRDS: !Equals [ 1, 1 ]

Resources:

  VPC:
    Type: AWS::CloudFormation::Stack
    Properties:
      TemplateURL: !Sub https://s3.amazonaws.com/${AppUrl}-cloudformation/infrastructure/vpc.yaml
      Parameters:
        EnvironmentName: !Ref AWS::StackName

  ALB:
    Type: AWS::CloudFormation::Stack
    Properties:
      TemplateURL: !Sub https://s3.amazonaws.com/${AppUrl}-cloudformation/infrastructure/load-balancers.yaml
      Parameters:
        EnvironmentName: !Ref AWS::StackName
        AppUrl: !Ref AppUrl
        HostedZoneId: !Ref HostedZoneId
        VPC: !GetAtt VPC.Outputs.VPC
        Subnets: !GetAtt VPC.Outputs.PublicSubnets
        WildcardSSLCertificateArn: !Ref WildcardSSLCertificateArn

  RDSPostgresDB:
    Type: AWS::CloudFormation::Stack
    Condition: UseRDS
    Properties:
      TemplateURL: !Sub https://s3.amazonaws.com/${AppUrl}-cloudformation/infrastructure/rds-postgres.yaml
      Parameters:
        VPC: !GetAtt VPC.Outputs.VPC
        PrivateSubnets: !GetAtt VPC.Outputs.PrivateSubnets
        SourceSecurityGroupId: !GetAtt ECS.Outputs.ContainerInstanceSecurityGroup

  ElastiCacheRedis:
    Type: AWS::CloudFormation::Stack
    Properties:
      TemplateURL: !Sub https://s3.amazonaws.com/${AppUrl}-cloudformation/infrastructure/elasticache-redis.yaml
      Parameters:
        VPC: !GetAtt VPC.Outputs.VPC
        PrivateSubnets: !GetAtt VPC.Outputs.PrivateSubnets
        SourceSecurityGroupId: !GetAtt ECS.Outputs.ContainerInstanceSecurityGroup

  ECS:
    Type: AWS::CloudFormation::Stack
    Properties:
      TemplateURL: !Sub https://s3.amazonaws.com/${AppUrl}-cloudformation/infrastructure/ecs-cluster.yaml
      Parameters:
        EnvironmentName: !Ref AWS::StackName
        InstanceType: t2.micro
        VPC: !GetAtt VPC.Outputs.VPC
        PublicSubnetOne: !GetAtt VPC.Outputs.PublicSubnetOne
        PublicSubnetTwo: !GetAtt VPC.Outputs.PublicSubnetTwo
        MaxSize: '2'
        DesiredCapacity: '1'
        ECSAMI: /aws/service/ecs/optimized-ami/amazon-linux/recommended/image_id
        SSHKeyName: !Ref SSHKeyName
        LoadBalancerSecurityGroup: !GetAtt ALB.Outputs.LoadBalancerSecurityGroup

  ServiceRole:
    Type: AWS::CloudFormation::Stack
    Properties:
      TemplateURL: !Sub https://s3.amazonaws.com/${AppUrl}-cloudformation/infrastructure/service-role.yaml

  ECRBackendRepository:
    Type: AWS::CloudFormation::Stack
    Properties:
      TemplateURL: !Sub https://s3.amazonaws.com/${AppUrl}-cloudformation/infrastructure/ecr-repository.yaml
      Parameters:
        ECRBackendRepositoryName: !Ref ECRBackendRepositoryName

  DjangoS3Storage:
    Type: AWS::CloudFormation::Stack
    Properties:
      TemplateURL: !Sub https://s3.amazonaws.com/${AppUrl}-cloudformation/infrastructure/django-assets.yaml
      Parameters:
        AppUrl: !Ref AppUrl
        StackName: !Ref StackName

  BackendService7:
    Type: AWS::CloudFormation::Stack
    Condition: RunServices
    Properties:
      TemplateURL: !Sub https://s3.amazonaws.com/${AppUrl}-cloudformation/services/backend.yaml
      Parameters:
        GitSHA: !Ref GitSHA
        DjangoSecretKey: !Ref DjangoSecretKey
        AWSAccessKeyId: !Ref AWSAccessKeyId
        AWSSecretAccessKey: !Ref AWSSecretAccessKey
        AppUrl: !Ref AppUrl
        VPC: !GetAtt VPC.Outputs.VPC
        ECSCluster: !GetAtt ECS.Outputs.ECSCluster
        DesiredCount: 1
        Listener: !GetAtt ALB.Outputs.Listener
        DBEndpoint: !GetAtt RDSPostgresDB.Outputs.DBEndpoint
        RedisEndpoint: !GetAtt ElastiCacheRedis.Outputs.RedisEndpoint
        ECRBackendRepositoryURL: !GetAtt ECRBackendRepository.Outputs.ECRBackendRepositoryURL
        ServiceRole: !GetAtt ServiceRole.Outputs.ServiceRole
        StackName: !Ref StackName

  DaphneService:
    Type: AWS::CloudFormation::Stack
    Condition: RunServices
    Properties:
      TemplateURL: !Sub https://s3.amazonaws.com/${AppUrl}-cloudformation/services/daphne.yaml
      Parameters:
        GitSHA: !Ref GitSHA
        DjangoSecretKey: !Ref DjangoSecretKey
        AWSAccessKeyId: !Ref AWSAccessKeyId
        AWSSecretAccessKey: !Ref AWSSecretAccessKey
        AppUrl: !Ref AppUrl
        VPC: !GetAtt VPC.Outputs.VPC
        ECSCluster: !GetAtt ECS.Outputs.ECSCluster
        DesiredCount: 1
        Listener: !GetAtt ALB.Outputs.Listener
        DBEndpoint: !GetAtt RDSPostgresDB.Outputs.DBEndpoint
        RedisEndpoint: !GetAtt ElastiCacheRedis.Outputs.RedisEndpoint
        ECRBackendRepositoryURL: !GetAtt ECRBackendRepository.Outputs.ECRBackendRepositoryURL
        ServiceRole: !GetAtt ServiceRole.Outputs.ServiceRole
        StackName: !Ref StackName

  CeleryService4:
    Type: AWS::CloudFormation::Stack
    Condition: RunServices
    Properties:
      TemplateURL: !Sub https://s3.amazonaws.com/${AppUrl}-cloudformation/services/celery.yaml
      Parameters:
        GitSHA: !Ref GitSHA
        DjangoSecretKey: !Ref DjangoSecretKey
        AWSAccessKeyId: !Ref AWSAccessKeyId
        AWSSecretAccessKey: !Ref AWSSecretAccessKey
        AppUrl: !Ref AppUrl
        VPC: !GetAtt VPC.Outputs.VPC
        ECSCluster: !GetAtt ECS.Outputs.ECSCluster
        DesiredCount: 1
        DBEndpoint: !GetAtt RDSPostgresDB.Outputs.DBEndpoint
        RedisEndpoint: !GetAtt ElastiCacheRedis.Outputs.RedisEndpoint
        ECRBackendRepositoryURL: !GetAtt ECRBackendRepository.Outputs.ECRBackendRepositoryURL
        ServiceRole: !GetAtt ServiceRole.Outputs.ServiceRole

  BeatService5:
    Type: AWS::CloudFormation::Stack
    Condition: RunServices
    Properties:
      TemplateURL: !Sub https://s3.amazonaws.com/${AppUrl}-cloudformation/services/beat.yaml
      Parameters:
        GitSHA: !Ref GitSHA
        DjangoSecretKey: !Ref DjangoSecretKey
        AWSAccessKeyId: !Ref AWSAccessKeyId
        AWSSecretAccessKey: !Ref AWSSecretAccessKey
        AppUrl: !Ref AppUrl
        VPC: !GetAtt VPC.Outputs.VPC
        ECSCluster: !GetAtt ECS.Outputs.ECSCluster
        DesiredCount: 1
        DBEndpoint: !GetAtt RDSPostgresDB.Outputs.DBEndpoint
        RedisEndpoint: !GetAtt ElastiCacheRedis.Outputs.RedisEndpoint
        ECRBackendRepositoryURL: !GetAtt ECRBackendRepository.Outputs.ECRBackendRepositoryURL
        ServiceRole: !GetAtt ServiceRole.Outputs.ServiceRole

  FlowerService1:
    Type: AWS::CloudFormation::Stack
    Condition: RunServices
    Properties:
      TemplateURL: !Sub https://s3.amazonaws.com/${AppUrl}-cloudformation/services/flower.yaml
      Parameters:
        VPC: !GetAtt VPC.Outputs.VPC
        ECSCluster: !GetAtt ECS.Outputs.ECSCluster
        Listener: !GetAtt ALB.Outputs.Listener
        DesiredCount: 1
        DBEndpoint: !GetAtt RDSPostgresDB.Outputs.DBEndpoint
        ECRBackendRepositoryURL: !GetAtt ECRBackendRepository.Outputs.ECRBackendRepositoryURL
        AppUrl: !Ref AppUrl
        RedisEndpoint: !GetAtt ElastiCacheRedis.Outputs.RedisEndpoint
        ServiceRole: !GetAtt ServiceRole.Outputs.ServiceRole
        FlowerUsername: !Ref FlowerUsername
        FlowerPassword: !Ref FlowerPassword

  FrontendResources:
    Type: AWS::CloudFormation::Stack
    Properties:
      TemplateURL: !Sub "https://s3.amazonaws.com/${AppUrl}-cloudformation/infrastructure/static-site.yaml"
      Parameters:
        AppUrl: !Ref AppUrl
        HostedZoneId: !Ref HostedZoneId
        SSLCertificateArn: !Ref SSLCertificateArn

Outputs:

  StackName:
    Description: The name of the stack
    Value: !Ref StackName
    Export:
      Name: StackName

  EnvironmentName:
    Description: An environemnt name to namespace resources (e.g. qa, staging, production)
    Value: !Ref EnvironmentName
    Export:
      Name: EnvironmentName

  AppUrl:
    Description: "The URL for our app (e.g. mydomain.com)"
    Value: !Ref AppUrl
    Export:
      Name: AppUrl

  HostedZoneId:
    Description: "The hosted zone ID to add the Route 53 recordset to."
    Value: !Ref HostedZoneId
    Export:
      Name: HostedZoneId

  SSLCertificateArn:
    Description: "ARN pointing to an SSL cert to use for this app URL."
    Value: !Ref SSLCertificateArn
    Export:
      Name: SSLCertificateArn

  WildcardSSLCertificateArn:
    Description: "ARN pointing to a wildcard SSL cert to use for this app URL."
    Value: !Ref WildcardSSLCertificateArn
    Export:
      Name: WildcardSSLCertificateArn

  ECRBackendRepositoryName:
    Description: "Name of ECR Repository for backend web server."
    Value: !Ref ECRBackendRepositoryName
    Export:
      Name: ECRBackendRepositoryName

  SSHKeyName:
    Description: The name of the key-pair used for SSH access to ECS hosts
    Value: !Ref SSHKeyName
    Export:
      Name: SSHKeyName

  GitSHA:
    Description: "This is a test parameter used to see how to pass parameters from GitLab"
    Value: !Ref GitSHA
    Export:
      Name: GitSHA
