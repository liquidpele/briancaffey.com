Description: >
  This service runs the Django webserver.

Parameters:

  GitSHA:
    Type: String
    Description: The Git SHA

  StackName:
    Type: String
    Description: The name of the stack

  AppUrl:
    Type: "String"
    Description: "The URL for our app (e.g. mydomain.com)"
    AllowedPattern: "[a-z0-9._-]+"

  VPC:
    Description: The VPC that the ECS cluster is deployed to
    Type: AWS::EC2::VPC::Id

  ECSCluster:
    Description: Please provide the ECS Cluster ID that this service should run on
    Type: String

  DesiredCount:
    Description: How many instances of this task should we run across our cluster?
    Type: Number
    Default: 2

  Listener:
    Description: The Application Load Balancer listener to register with
    Type: String

  DBEndpoint:
    Description: The DB Endpoint
    Type: String

  RedisEndpoint:
    Description: The Redis Endpoint
    Type: String

  ServiceRole:
    Description: An IAM Role that grants the service access to register/unregister with the Application Load Balancer (ALB).
    Type: String

  ECRBackendRepositoryURL:
    Description: The ECR repository for the backend container
    Type: String

  AWSAccessKeyId:
    Description: "AWS ACCESS KEY ID"
    Type: String

  AWSSecretAccessKey:
    Description: "AWS SECRET ACCESS KEY"
    Type: String

  DjangoSecretKey:
    Description: The Secret Key for backend, celery and beat containers
    Type: String
    NoEcho: true

Resources:
  BackendService:
    Type: AWS::ECS::Service
    DependsOn: ListenerRule
    Properties:
      Cluster: !Ref ECSCluster
      Role: !Ref ServiceRole
      DesiredCount: !Ref DesiredCount
      TaskDefinition: !Ref TaskDefinition
      LoadBalancers:
        - ContainerName: "backend"
          ContainerPort: 8000
          TargetGroupArn: !Ref BackendTargetGroup

  TaskDefinition:
    Type: AWS::ECS::TaskDefinition
    Properties:
      Family: backend
      ContainerDefinitions:
        - Name: backend
          Essential: true
          Image: !Sub ${ECRBackendRepositoryURL}:${GitSHA}
          MemoryReservation: 128
          Command:
            - '/start_prod.sh'
          Environment:
            - Name: GIT_SHA
              Value: !Ref GitSHA
            - Name: AWS_ACCESS_KEY_ID
              Value: !Ref AWSAccessKeyId
            - Name: AWS_SECRET_ACCESS_KEY
              Value: !Ref AWSSecretAccessKey
            - Name: SECRET_KEY
              Value: !Ref DjangoSecretKey
            - Name: APP_URL
              Value: !Ref AppUrl
            - Name: DEBUG
              Value: ''
            - Name: RDS_DB_NAME
              Value: postgres
            - Name: RDS_USERNAME
              Value: postgres
            - Name: RDS_PASSWORD
              Value: postgres
            - Name: RDS_HOSTNAME
              Value: !Ref DBEndpoint
            - Name: RDS_PORT
              Value: 5432
            - Name: CELERY_BROKER_URL
              Value: !Ref RedisEndpoint
            - Name: CELERY_RESULT_BACKEND
              Value: !Ref RedisEndpoint
            - Name: STACK_NAME
              Value: !Ref StackName
          PortMappings:
            - ContainerPort: 8000
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-group: !Ref BackendCloudWatchLogsGroup
              awslogs-region: !Ref AWS::Region

  BackendTargetGroup:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    Properties:
      VpcId: !Ref VPC
      Port: 80
      Protocol: HTTP
      Matcher:
        HttpCode: 200-299
      HealthCheckIntervalSeconds: 300
      HealthCheckPath: /api/hello-world
      HealthCheckProtocol: HTTP
      HealthCheckTimeoutSeconds: 5
      HealthyThresholdCount: 2

  ListenerRule:
    Type: AWS::ElasticLoadBalancingV2::ListenerRule
    Properties:
      ListenerArn: !Ref Listener
      Priority: 3
      Conditions:
        - Field: path-pattern
          Values:
            - "*"
      Actions:
        - TargetGroupArn: !Ref BackendTargetGroup
          Type: forward

  BackendCloudWatchLogsGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: !Ref AWS::StackName
      RetentionInDays: 365
